# plantuml

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```

```plantuml
Object <|-- ArrayList

Object : equals()
ArrayList : Object[] elementData
ArrayList : size()
```


```plantuml
class Dummy {
  String data
  void methods()
}

class Flight {
   flightNumber : Integer
   departureTime : Date
}
```

```plantuml

@startuml
!pragma ratio 0.5
'skinparam padding 0
'skinparam nodesep 0
!theme spacelab

actor Developer

cloud EF as "EF cloud" {
  node gitef as "gitlab.eclipse.org"
  database artef as "Artifacts"
  gitef<->artef
}

cloud OSTC as "Oniro OSTC lab" {
  node dell as "Dell server" {
    component lxd1  as "LXD container" {
      component macaque as "gitlab-runner1" #yellowgreen
    }

    database stor1 as "local cache" #orange


    component lxd2 as "LXD container" {
      component champion as "gitlab-runner2" #yellowgreen
    }
    macaque-right->stor1
    champion-down->stor1
  }

  cloud hlaas as "Hardware Lab" {
    node rdut1 as "Raspberry Pi 4B"{
      component lavaw1 as "Lava worker"
    }
    node dut1 as "dut1"
    node dutN as "dutN"
  
    lavaw1-->dut1
    lavaw1-->dutN
    actor hadmin as "Hardware Admin"

    note bottom of hadmin
     Frequent hands-on activity
     on Devices Under Tests
    end note
  }

  note bottom of hlaas
   Multiple instances
  end note

  note bottom of stor1
    /cache
    /var/shared
    But in fact this is not shared.
    Each gitlab-runner has its own cache.
  end note
}

cloud OTC as "Open Telekom Cloud" {
  node ecs1 as "ECS server"{
    component gitotc as "git.ostc-eu.org" 
      database artotc as "gitlab artifacts"
      database regotc as "registry.git.ostc-eu.org"#orange
      gitotc-l->artotc
      gitotc->regotc
  }

  port otcapi as "OTC API"

  node docker1 as "ECS docker" {
    component comp1 as "docker compose"{
      component grafana as "grafana.ostc-eu.dev"
      component hawkbit as "hawkbit.ostc-eu.dev"
      component squad as "squadp.ostc-eu.dev"
    }
  }

  node ecsr1 as "ECS server"{
    component runner1 as "gitlab-runner 1" #yellowgreen
  }

  node rm as "ECS server" {
    component grm as "gitlab runner manager"#yellowgreen
    database cache as "/srv/cache" #yellowgreen
    grm->cache
  }

  note bottom of cache
   NFS export
   1TB /srv/cache
  end note

  note bottom of grm
    Orchestration of ECS servers deployment and deletion
  end note
  
  node ecsr2 as "ECS server"{
    component runnern as "gitlab-runner N" #yellowgreen
  }
  database s3 as "S3 storage"
  database s31 as "S3 storage"

  node lava as "lava.ostc-eu.org" {
    component lavas as "lava server"
  }
  runner1->cache
  runnern->cache
}

EF-[hidden]-->OTC

Developer-->gitef
gitef-->squad
squad-->lavas
gitef-->macaque
gitef-->champion
gitef-->hawkbit
hawkbit-d->s31
lavas->lavaw1
gitotc-d->s3

gitef-->regotc
gitef-->grm
grm-down->otcapi

gitef-->runner1
gitef-->runnern

@enduml

```

